ifneq ($(TARGET_SIMULATOR),true)

LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_C_INCLUDES += $(TARGET_OUT_INTERMEDIATES)/KERNEL_OBJ/usr/include
LOCAL_ADDITIONAL_DEPENDENCIES := $(TARGET_OUT_INTERMEDIATES)/KERNEL_OBJ/usr

LOCAL_SRC_FILES := cimax.c \
		cimax_os_glue.c \
		cimax_spi_pio.c

LOCAL_MODULE := libcimax_spi
LOCAL_SHARED_LIBRARIES := libcutils libc liblog libdl
LOCAL_MODULE_TAGS:= optional
LOCAL_PRELINK_MODULE := false
include $(BUILD_SHARED_LIBRARY)

endif  # TARGET_SIMULATOR != true
