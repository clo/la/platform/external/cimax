/*==========================================================================
Copyright (c) 2012, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation, nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***************************************************************************/

/**************************************************************************

                            INCLUDE FILES FOR MODULE

**************************************************************************/
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <linux/ci-bridge-spi.h>
#include "cimax_spi_pio.h"

static int spi_fd = -1;

/**************************************************************************

                             FUNCTION DEFINITIONS

**************************************************************************/

/**
FUNCTION: CIMAX_SPI_Init

@brief
  Initialize SPI driver.

@return
  0 on success, nonzero value otherwise.

@note
  None
@callgraph
@callergraph
**************************************************************************/
int32 CIMAX_SPI_Init(spiInitParams_t *pSpiInitParams)
{
  if (NULL == pSpiInitParams)
  {
    return -1;
  }

  spi_fd = open("/dev/ci_bridge_spi0", O_RDWR);

  if (spi_fd < 0)
  {
    return -1;
  }

  /* Assume dedicated CIMAX SPI device driver is already configured to work with the required parameters */
  /*
  ret = ioctl(spi_fd, SPI_IOC_WR_MODE, &pSpiInitParams->mode);
  if (0 != ret)
  {
    close(spi_fd);
    return ret;
  }

  ret = ioctl(spi_fd, SPI_IOC_WR_MAX_SPEED_HZ, &pSpiInitParams->clock);
  if (0 != ret)
  {
    close(spi_fd);
    return ret;
  }
  */

  return 0;
}

/**
FUNCTION: CIMAX_SPI_Write

@brief
  Write a given number of bytes from a given buffer over the SPI bus.

@return
  0 on success (all bytes were written), nonzero value otherwise.

@note
  None
@callgraph
@callergraph
**************************************************************************/
int32 CIMAX_SPI_Write(uint8 *buff, uint32 numOfBytes)
{
  uint32 numWritten = 0;

  if ((0 == numOfBytes) || (spi_fd < 0))
  {
    return -1;
  }

  numWritten = write(spi_fd, buff, numOfBytes);
  if (numWritten == numOfBytes)
  {
    return 0;
  }

  return -1;
}

/**
FUNCTION: CIMAX_SPI_Read

@brief
  Read a given number of bytes to a given buffer via the SPI bus.

@return
  Number of bytes actually read.

@note
  None
@callgraph
@callergraph
**************************************************************************/
int32 CIMAX_SPI_Read(uint8 *buff, uint32 numOfBytes)
{
  if ((0 == numOfBytes) || (spi_fd < 0))
  {
    return 0;
  }

  return read(spi_fd, buff, numOfBytes);
}

/**
FUNCTION: CIMAX_SPI_Term

@brief
  Un-initialize SPI driver.

@return
  0 on success, nonzero value otherwise.

@note
  None
@callgraph
@callergraph
**************************************************************************/
int32 CIMAX_SPI_Term()
{
  int32 ret;
  if (spi_fd < 0)
    return -1;

  ret = close(spi_fd);
  spi_fd = -1;
  return ret;
}

/**
FUNCTION: CIMAX_PIO_Init

@brief
  Initialize PIO access driver.

@return
  0 on success, nonzero value otherwise.

@note
  None
@callgraph
@callergraph
**************************************************************************/
int32 CIMAX_PIO_Init()
{
  return CIMAX_PIO_Reset();
}

/**
FUNCTION: CIMAX_PIO_Reset

@brief
  Reset the CIMAX bridge.

@return
  0 on success, nonzero value otherwise.

@note
  None
@callgraph
@callergraph
**************************************************************************/
int32 CIMAX_PIO_Reset()
{
  if (spi_fd < 0)
    return -1;

  ioctl(spi_fd, CI_BRIDGE_IOCTL_RESET, 1);
  usleep(10);
  ioctl(spi_fd, CI_BRIDGE_IOCTL_RESET, 0);

  return 0;
}

/**
FUNCTION: CIMAX_PIO_GetIntState

@brief
  Read value of interrupt state pin.

@return
  0 or 1 depending on interrupt state pin value.

@note
  None
@callgraph
@callergraph
**************************************************************************/
int32 CIMAX_PIO_GetIntState(uint8 *pState)
{
  int ret = 0;
  int state = 0;

  if (spi_fd >= 0)
    ret = ioctl(spi_fd, CI_BRIDGE_IOCTL_GET_INT_STATE, &state);
  else
    ret = -1;

  if (0 == ret)
  {
    *pState = state;
  }
  else
  {
    /* nothing much we can do if the caller doesn't check the return value, */
    /* but we set *pState to the default CIMaX+ INT pin state which is 1. */
    *pState = 1;
  }

  return ret;
}

/**
FUNCTION: CIMAX_PIO_Term

@brief
  Un-initialize PIO access driver.

@return
  0 on success, nonzero value otherwise.

@note
  None
@callgraph
@callergraph
**************************************************************************/
int32 CIMAX_PIO_Term()
{
  if (spi_fd < 0)
    return -1;

  ioctl(spi_fd, CI_BRIDGE_IOCTL_RESET, 1);
  return 0;
}

